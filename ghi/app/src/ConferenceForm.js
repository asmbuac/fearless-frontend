import React, { useEffect, useState } from 'react'
import useFetch from './useFetch';

export default function ConferenceForm() {
  const locations = useFetch('http://localhost:8000/api/locations/', 'locations');
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [location, setLocation] = useState('');

  const handleNameChange = (event) => {
    setName(event.target.value);
  }

  const handleStartsChange = (event) => {
    setStarts(event.target.value);
  }

  const handleEndsChange = (event) => {
    setEnds(event.target.value);
  }

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  }

  const handleMaxPresentationsChange = (event) => {
    setMaxPresentations(event.target.value);
  }

  const handleMaxAttendeesChange = (event) => {
    setMaxAttendees(event.target.value);
  }

  const handleLocationChange = (event) => {
    setLocation(event.target.value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name,
      starts,
      ends,
      description,
      max_presentations: maxPresentations,
      max_attendees: maxAttendees,
      location,
    };

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentations('');
      setMaxAttendees('');
      setLocation('');
    }

  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={starts} onChange={handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
              <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input value={ends} onChange={handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
              <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea value={description} onChange={handleDescriptionChange} required name="description" id="description" className="form-control" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input value={maxPresentations} onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="number" name="max_presentations"
                id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees"
                className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="form-floating mb-3">
              <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="location">Location</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
