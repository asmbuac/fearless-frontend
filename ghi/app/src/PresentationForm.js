import React, { useEffect, useState } from 'react'
import useFetch from './useFetch';

export default function PresentationForm() {
  const conferences = useFetch('http://localhost:8000/api/conferences/', 'conferences');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conference, setConference] = useState('');

  const handleNameChange = (event) => {
    setName(event.target.value);
  }

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  }

  const handleCompanyNameChange = (event) => {
    setCompanyName(event.target.value);
  }

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  }

  const handleSynopsisChange = (event) => {
    setSynopsis(event.target.value);
  }

  const handleConferenceChange = (event) => {
    setConference(event.target.value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      presenter_name: name,
      presenter_email: email,
      company_name: companyName,
      title,
      synopsis,
      conference,
    };

    const url = `http://localhost:8000/api/conferences/${conference}/presentations/`;
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newPresentation = await response.json();
      setName('');
      setEmail('');
      setCompanyName('');
      setTitle('');
      setSynopsis('');
      setConference('');
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name"
                className="form-control" />
              <label htmlFor="presenter_name">Presenter name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={email} onChange={handleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email"
                className="form-control" />
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input value={companyName} onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name"
                className="form-control" />
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={title} onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea value={synopsis} onChange={handleSynopsisChange} required name="synopsis" id="synopsis" className="form-control" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
              <select value={conference} onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                  return (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="conference">Conference</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
